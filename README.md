# Gentle Monokai

Forked from [One Monokai theme by azenoh](https://github.com/azemoh/vscode-one-monokai)

## Install

press `ctl/command + shift + p` to launch the command palette then run
```
ext install gentle-monokai
```
